INTRODUCTION
------------
Moderate mmenu provides an off-screen menu to moderate the current Node.

By default no blocks are added to the menu, but users are expected to move
local tasks there as well as the Workbench Moderation block (once it is ported).

REQUIREMENTS
------------
This module requires the jQuery.mmenu library, see INSTALL.txt for details.

CONFIGURATION
-------------
Beyond assigning blocks to the custom region, there are no extra configuration
options available.
